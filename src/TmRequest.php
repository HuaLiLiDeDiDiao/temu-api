<?php

namespace Temu;
class TmRequest
{
    public $apiType;

    public $headerParams = array();

    public $udfParams = array();

    public $fileParams = array();


    public function __construct($apiType)
    {
        $this->apiType = $apiType;
    }


    function addApiParam($key, $value)
    {

        if (!is_string($key)) {
            throw new Exception("api param key should be string");
        }

        if (is_object($value)) {
            $this->udfParams[$key] = json_decode($value);
        } else {
            $this->udfParams[$key] = $value;
        }
    }

    function addFileParam($key, $content, $mimeType = 'application/octet-stream')
    {
        if (!is_string($key)) {
            throw new Exception("api file param key should be string");
        }

        $file = array(
            'type' => $mimeType,
            'content' => $content,
            'name' => $key
        );
        $this->fileParams[$key] = $file;
    }

    function addHttpHeaderParam($key, $value)
    {
        if (!is_string($key)) {
            throw new Exception("http header param key should be string");
        }

        if (!is_string($value)) {
            throw new Exception("http header param value should be string");
        }

        $this->headerParams[$key] = $value;
    }
}

?>